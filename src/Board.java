
public class Board {
	private char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' }, };
	private Player x;
	private Player o;
	private Player winner;
	private Player current;
	private int turnCount;

	public Board(Player x, Player o) {
		this.x = x;
		this.o = o;
		current = x;
		winner = null;
		turnCount = 0;
	}

	public char[][] getTable() {
		return table;
	}

	public Player getCurrent() {
		return current;
	}

	public boolean setTable(int row, int col) {
		if (table[row][col] == '-') {
			table[row][col] = current.getName();
			return true;
		}
		return false;
	}

	public boolean checkRow(int row) {
		for (int col = 0; col < table[row].length; col++) {
			if (table[row][col] != current.getName()) {
				return false;
			}
		}
		return true;
	}

	public boolean checkRow() {
		if (checkRow(0) || checkRow(1) || checkRow(2)) {
			return true;
		}
		return false;
	}

	public boolean checkCol(int col) {
		for (int row = 0; row < table.length; row++) {
			if (table[row][col] != current.getName()) {
				return false;
			}
		}
		return true;
	}

	public boolean checkCol() {
		if (checkCol(0) || checkCol(1) || checkCol(2)) {
			return true;
		}
		return false;
	}

	public boolean checkX1() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][i] != current.getName()) {
				return false;
			}
		}
		return true;
	}

	public boolean checkX2() {
		for (int i = 0; i < table.length; i++) {
			if (table[2-i][i] != current.getName()) {
				return false;
			}
		}
		return true;
	}

	public boolean checkDraw() {
		if(turnCount==8) {
			x.draw();
			o.draw();
			return true;
		}
		return false;
	}

	public boolean checkWin() {
		if (checkRow() || checkCol() || checkX1() || checkX2()) {
			winner = current;
			if(current==x) {
				x.win();
				o.lose();
			}else {
				o.win();
				x.lose();
			}
			return true;
		}
		return false;

	}
	
	public Player getWinner() {
		return winner;
	}

	public boolean isFinish() {
		if (checkWin()) {
			return true;
		}
		if (checkRow()) {
			return true;
		}
		if (checkCol()) {
			return true;
		}
		if (checkX1()) {
			return true;
		}
		if (checkX2()) {
			return true;
		}
		if (checkDraw()) {
			return true;
		}
		return false;
	}

	public void switchTurn() {
		if (current == x) {
			current = o;
		} else {
			current = x;
		}
		turnCount++;
	}

}
